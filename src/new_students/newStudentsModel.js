const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const newStudentsSchema = new Schema(
    {
        class_id: {
            type: Schema.Types.ObjectId,
            ref: "classes",
            required: true
        },
        name: {
            type: String,
            required: true
        },
        contact_number: {
            type: String,
            required: true,
        },
        email: {
            type: String,
            required: true,
        },
        javascript_knowledge: {
            type: Number,
            required: true,
        },
        java_knowledge: {
            type: Number,
            required: true,
        }
        ,
        is_confirmed: {
            type: String,
            required: true,
        }
        ,
        confirm_student: {
            type: String,
            required: false,
        }
    }, { timestamps: true }
);
// newStudentsSchema.index({ classes: "2dsphere" });
module.exports = mongoose.model('new_students', newStudentsSchema);
