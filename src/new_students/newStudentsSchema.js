const joi = require('joi');

// updateValve registration Schema and validations to be done
module.exports.create = joi.object().keys({
  name: joi.string().required(),
  class_id: joi.string().required(),
  contact_number: joi.string().required(),
  javascript_knowledge: joi.number().required(),
  email: joi.string().email().required(),
  java_knowledge: joi.number().required(),
  is_confirmed:joi.string().required()
});

// mainValveID registration Schema and validations to be done
module.exports.id = joi.object().keys({
  _id: joi.string().alphanum().min(24).max(24)
});


// mainValveStatus registration Schema and validations to be done
module.exports.update = joi.object().keys({
  name: joi.string(),
  class_id: joi.string(),
  contact_number: joi.string(),
  javascript_knowledge: joi.number(),
  email: joi.string().email(),
  java_knowledge: joi.number(),
  is_confirmed:joi.string()
});