const newStudentsModel = require('./newStudentsModel');
/**
 * find from the system
 */
module.exports.getAll = (condition) => {
    return new Promise((resolve, reject) => {
        newStudentsModel.find(condition).populate('class_id').exec((err, classes) => {
            err ? reject(err) : resolve(classes);
        });
    })
};

/**
 * remove  from the system
 */
module.exports.delete = (id) => {
    return new Promise((resolve, reject) => {
        newStudentsModel.findOneAndDelete({ _id: id },
            async (err, classes) => {
                if (err)
                    reject(err);
                else if (classes != null || classes != undefined) {
                    resolve("successfully deleted");
                } else
                    reject("Invalid id");
            });
    })
}
/**
 * register new to the system
 */
module.exports.create = async (body) => {
    return new Promise((resolve, reject) => {
        newStudentsModel.findOne({ email: body.email }, async function (err, classesData) {
            console.log(classesData);
            console.log(err);
            if (err) {
                reject(err);
            }
            else if (classesData == undefined || classesData == null) {
                const newStudent = new newStudentsModel();
                newStudent.name = body.name;
                newStudent.class_id = body.class_id;
                newStudent.email = body.email;
                newStudent.contact_number = body.contact_number;
                newStudent.javascript_knowledge = body.javascript_knowledge;
                newStudent.java_knowledge = body.java_knowledge;
                newStudent.is_confirmed  = body.is_confirmed;
                newStudent.save(async (error, classesCb) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve(classesCb);
                    }
                });
            } else {
                reject("Student already exists");
            }
        });
    });
}

/**
 * find  from the system by id
 */
module.exports.findOne = (id) => {
    return new Promise((resolve, reject) => {
        newStudentsModel.findOne({ _id: id }).populate('class_id')
            .exec((err, data) => {
                err ? reject(err) : resolve(data);
            });
    })
};


/**
 * find and update from the system by id
 */
module.exports.findByIdAndUpdate = async (id, updateObject) => {
    return new Promise(async (resolve, reject) => {
        try {
            newStudentsModel.findOne({ _id: id }, function (err1, classesData) {
                console.log(id);
                console.log(updateObject);
                if (err1) {
                    reject(err1);
                }
                else if (classesData != undefined || classesData != null) {
                    newStudentsModel.update({ _id: id }, { $set: updateObject }).exec((err, data) => {
                        err ? reject(err) : resolve(data);
                    });
                } else {
                    reject("classes ID not found");
                }

            });
        } catch (error) {
            reject("Something went wrong");
        }
    })
};