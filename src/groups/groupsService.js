const groupsModel = require('./groupsModel');
/**
 * find from the system
 */
module.exports.getAll = (condition) => {
    return new Promise((resolve, reject) => {
        groupsModel.find(condition).populate('class_id').populate('mentor').populate({ path: 'students.detail', populate: { path: 'classes.class_id' } }).populate({ path: 'students.detail', populate: { path: 'classes.group_id' } }).exec((err, groups) => {
            err ? reject(err) : resolve(groups);
        });
    })
};

/**
 * remove  from the system
 */
module.exports.delete = (id) => {
    return new Promise((resolve, reject) => {
        groupsModel.findOneAndDelete({ _id: id },
            async (err, groups) => {
                if (err)
                    reject(err);
                else if (groups != null || groups != undefined) {
                    resolve("successfully deleted");
                } else
                    reject("Invalid id");
            });
    })
}

/**
 * remove  from the system
 */
module.exports.deleteStudentFromGroup = (body) => {
    return new Promise((resolve, reject) => {
        groupsModel.update(
            { _id: body.group_id },
            { $pull: { students: { detail: body.student_id } } },
            async (err, groups) => {
                if (err) {
                    reject(err);
                }
                else if (groups != null || groups != undefined) {
                    resolve("successfully deleted");
                }
            });
    });
}
/**
 * remove  from the system
 */
module.exports.deleteStudentFromGroupInDeleteState = (body) => {
    return new Promise((resolve, reject) => {
        groupsModel.update(
            { _id: body.group_id },
            { $pull: { students: { _id: body.student_id } } },
            { multi: true },
            async (err, groups) => {
                if (err) {
                    reject(err);
                }
                else if (groups != null && groups != undefined) {
                    resolve("successfully deleted");
                }else{
                    reject("Somthing went wrong..!");
                }
            });
    });
}

/**
 * register new to the system
 */
module.exports.create = async (body) => {
    return new Promise((resolve, reject) => {
        groupsModel.findOne({ name: body.name }, async function (err, groupsData) {
            console.log(groupsData);
            console.log(err);
            if (err) {
                reject(err);
            }
            else if (groupsData == undefined || groupsData == null) {
                const groups = new groupsModel();
                groups.name = body.name;
                groups.class_id = body.class_id;
                groups.group_id = body.group_id;
                groups.mentor = body.mentor;
                groups.group_status = 'ACTIVE';
                groups.price = body.price;
                groups.students = (body.students && body.students.length > 0) ? body.students : [];
                groups.mentor_payment_status = 'N';
                groups.save(async (error, groupsCb) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve(groupsCb);
                    }
                });
            } else {
                reject("groups already exists");
            }
        });
    });
}

/**
 * find  from the system by id
 */
module.exports.findOne = (id) => {
    return new Promise((resolve, reject) => {
        locationModel.findOne({ _id: id }).populate('class_id').populate('mentor').populate('students')
            .exec((err, data) => {
                err ? reject(err) : resolve(data);
            });
    })
};


/**
 * find and update from the system by id
 */
module.exports.findByIdAndUpdate = async (id, updateObject) => {
    return new Promise(async (resolve, reject) => {
        try {
            groupsModel.findOne({ _id: id }, function (err1, groupsData) {
                console.log(id);
                console.log(updateObject);
                if (err1) {
                    reject(err1);
                }
                else if (groupsData != undefined || groupsData != null) {
                    groupsModel.update({ _id: id }, { $set: updateObject }).exec((err, data) => {
                        err ? reject(err) : resolve(data);
                    });
                } else {
                    reject("groups ID not found");
                }

            });
        } catch (error) {
            reject("Something went wrong");
        }
    })
};