const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const groupsSchema = new Schema(
    {
        group_id: {
            type: String,
            required: true
        },
        name: {
            type: String,
            required: true
        },
        group_status: {
            type: String,
            required: true
        },
        price: {
            type: Number,
            required: true
        },
        class_id: {
            type: Schema.Types.ObjectId,
            ref: "classes"
        },
        mentor: {
            type: Schema.Types.ObjectId,
            ref: "User"
        },
        students: [{
            detail: {
                type: Schema.Types.ObjectId,
                ref: "students"
            },
            payment_status: {
                type: String,
                required: false
            },
            payment_amount: {
                type: Number,
                required: false
            }
        }],
        mentor_payment_status: {
            type: String,
            required: false
        },
    }, { timestamps: true }
);
// groupsSchema.index({ groups: "2dsphere" });
module.exports = mongoose.model('groups', groupsSchema);
