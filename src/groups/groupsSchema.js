const joi = require('joi');

// updateValve registration Schema and validations to be done
module.exports.create = joi.object().keys({
  group_id: joi.string().required(),
  class_id: joi.string().alphanum().min(24).max(24).required(),
  name: joi.string().required(),
  price: joi.number().required(),
  mentor: joi.string().alphanum().min(24).max(24).required(),
  students: joi.array().items(),
});

// mainValveID registration Schema and validations to be done
module.exports.id = joi.object().keys({
  _id: joi.string().alphanum().min(24).max(24)
});

// mainValveID registration Schema and validations to be done
module.exports.removeFromGroup = joi.object().keys({
  student_id: joi.string().alphanum().min(24).max(24),
  group_id: joi.string().alphanum().min(24).max(24)
});


// mainValveStatus registration Schema and validations to be done
module.exports.update = joi.object().keys({
  class_id: joi.string().alphanum().min(24).max(24),
  name: joi.string(),
  group_status: joi.string(),
  price: joi.number(),
  mentor: joi.string().alphanum().min(24).max(24),
  students: joi.array().items(),
  mentor_payment_status: joi.string()
});