const groupsService = require('./groupsService');
const response = require('../../services/responseService');


/**
 * get all 
 */
module.exports.getAll = async (req, res) => {
    try {
        console.log(req.query);
        let items = await groupsService.getAll(req.query);
        response.successWithData(items, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}

/**
 * get one item
 */
module.exports.findOne = async (req, res) => {
    try {
        let item = await groupsService.findOne(req.params.id)
        response.successWithData(item, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}

/**
 * create item
 */
module.exports.create = async (req, res) => {
    try {
        let item = await groupsService.create(req.body);
        response.successWithData(item, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}

/**
 * update item by id
 * @param {*} req
 * @param {*} res
 */
module.exports.update = async (req, res) => {
    try {
        let details = JSON.parse(JSON.stringify(req.body));
        let item = await groupsService
            .findByIdAndUpdate(req.params.id, details);
        if (item == null || item == undefined)
            response.customError("Invalid id", res)
        else
            response.successWithData(item, res);
    } catch (error) {
        response.customError('' + error, res);
    }
};

/**
 * remove item by id
 * @param {*} req
 * @param {*} res
 */
module.exports.remove = async (req, res) => {
    try {
        console.log(req.params.id);
        let item = await groupsService.delete(req.params.id);
        response.successWithData(item, res);
    } catch (error) {
        response.customError('' + error, res);
    }
};
/**
 * remove item by id
 * @param {*} req
 * @param {*} res
 */
module.exports.removeFromGroup = async (req, res) => {
    try {
        console.log(req.query);
        let item = await groupsService.deleteStudentFromGroup(req.query);
        response.successWithData(item, res);
    } catch (error) {
        response.customError('' + error, res);
    }
};

/**
 * remove item by id
 * @param {*} req
 * @param {*} res
 */
module.exports.removeFromGroupStudentInDeleteState = async (req, res) => {
    try {
        console.log(req.query);
        let item = await groupsService.deleteStudentFromGroupInDeleteState(req.query);
        response.successWithData(item, res);
    } catch (error) {
        response.customError('' + error, res);
    }
};