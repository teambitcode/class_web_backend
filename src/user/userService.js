const userModel = require('./userModel');

/**
 * create new user
 * @param {*} body 
 */
module.exports.createUser = (body) => {
    console.log("In Service");
    console.log(body);
    console.log("In Service");
    return new Promise((resolve, reject) => {
        userModel.findOne({ email: body.email }, function (err, userData) {
            console.log(userData);
            console.log(err);
            if (err) {
                reject(err);
            }
            else if (userData == undefined || userData == null) {
                const user = new userModel();
                user.email = body.email;
                user.role = body.role;
                user.first_name = body.first_name ? body.first_name: '';
                user.last_name = body.last_name ? body.last_name: '';
                user.bank_name = body.bank_name ? body.bank_name: '';
                user.bank_account_number = body.bank_account_number ? body.bank_account_number: '';
                user.name_in_bank_account = body.name_in_bank_account ? body.name_in_bank_account: '';
                user.contact_number = body.contact_number ? body.contact_number: '';
                user.setPassword(body.password);
                user.save((error, user) => {
                    (error) ? reject(error) : resolve(user);
                })
            } else {
                reject("User already exists");
            }
        })
    })
}
module.exports.updatePassword = async (id, updateObject) => {
    return new Promise(async (resolve, reject) => {
        try {
            const user = new userModel();
            user.setPassword(updateObject.password);
            userModel.findOne({ _id: id }, function (err1, userData) {
                console.log(id);
                console.log(updateObject);
                if (err1) {
                    reject(err1);
                }
                else if (userData != undefined || userData != null) {
                    userModel.update({ _id: id }, { $set: {password:user.password,salt:user.salt} }).exec((err, data) => {
                        err ? reject(err) : resolve(data);
                    });
                } else {
                    reject("User ID not found");
                }

            });
        } catch (error) {
            reject("Something went wrong..!");
        }
    })
};
module.exports.getAll = (condition) => {
    return new Promise((resolve, reject) => {
        userModel.find(condition).exec((err, user) => {
            err ? reject(err) : resolve(user);
        });
    })
};
module.exports.getOne = (condition) => {
    return new Promise((resolve, reject) => {
        userModel.findOne(condition).exec((err, user) => {
            err ? reject(err) : resolve(user);
        });
    })
};

/**

/**
 * login user 
 * @param {*} usser 
 */
module.exports.loginUser = (user) => {
    return new Promise((resolve, reject) => {
        try {
            console.log(user);
            userModel.findOne({ email: user.email }, function (err, userData) {
                if (err) {
                    reject(err);
                }
                if (userData != undefined || userData != null) {
                    let status = userData
                        .validPassword(user.password);
                    (status) ?
                        resolve({status:true,role:userData.role,id:userData._id}) :
                        reject("Invalid user name or password");
                } else {
                    reject("Invalid user name or password");
                }
            })
        } catch (error) {
            console.log(error);
            reject('' + error);
        }
    })
}

/**
 * find and update from the system by id
 */
module.exports.findByIdAndUpdate = async (id, updateObject) => {
    return new Promise(async (resolve, reject) => {
        try {
            userModel.findOne({ _id: id }, function (err1, userData) {
                console.log(id);
                console.log(updateObject);
                if (err1) {
                    reject(err1);
                }
                else if (userData != undefined || userData != null) {
                    userModel.update({ _id: id }, { $set: updateObject }).exec((err, data) => {
                        err ? reject(err) : resolve(data);
                    });
                } else {
                    reject("User ID not found");
                }

            });
        } catch (error) {
            reject("Something went wrong..!");
        }
    })
};


/**
 * remove  from the system
 */
module.exports.delete = (id) => {
    return new Promise((resolve, reject) => {
        userModel.findOneAndDelete({ _id: id },
            async (err, user) => {
                if (err)
                    reject(err);
                else if (user != null || user != undefined) {
                    resolve("successfully deleted");
                } else
                    reject("Invalid id");
            });
    })
}