// import validator class
const joi = require('joi');

// user scorce Schema and validations to be done
module.exports.login = joi.object().keys({
    email: joi
        .string()
        .email()
        .required(),
    password: joi.required(),
});
// user registration Schema and validations to be done
module.exports.newUser = joi.object().keys({
    email: joi
        .string()
        .email()
        .required(),
    password: joi.string()
        .min(6)
        .required(),
    first_name: joi
        .string(),
    last_name: joi
        .string(),
    bank_name: joi
        .string(),
    bank_account_number: joi
        .string(),
    name_in_bank_account: joi
        .string(),
    contact_number: joi
        .string(),
        role: joi
        .string(),
});
module.exports.update = joi.object().keys({
    email: joi
        .string()
        .email(),
    role: joi
        .string(),
    first_name: joi
        .string(),
    last_name: joi
        .string(),
    bank_name: joi
        .string(),
    bank_account_number: joi
        .string(),
    name_in_bank_account: joi
        .string(),
    contact_number: joi
        .string()
});

module.exports.id = joi.object().keys({
    _id: joi.string().alphanum().min(24).max(24)
});
module.exports.password = joi.object().keys({
    password: joi.string().required()
});