const crypto = require('crypto');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// create user Schema
const UserSchema = new Schema(
    {   email: {
            type: String,
            required: true,
        },
        first_name: {
            type: String,
            required: false,
        },
        last_name: {
            type: String,
            required: false,
        },
        password: {
            type: String,
            required: true,
        },
        role: {
            type: String,
            required: true,
        },
        bank_name: {
            type: String,
            required: true,
        },
        bank_account_number: {
            type: String,
            required: true,
        },
        name_in_bank_account: {
            type: String,
            required: true,
        },
        contact_number: {
            type: String,
            required: true,
        },
        salt: String,
    },
    { timestamps: true }
);

// validate password with the database
UserSchema.methods.validPassword = function (password) {
    const hash = crypto
        .pbkdf2Sync(password, this.salt, 10000, 512, 'sha512')
        .toString('hex');
    return this.password === hash;
};
// encrypt the password before save
UserSchema.methods.setPassword = function (password) {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.password = crypto
        .pbkdf2Sync(password, this.salt, 10000, 512, 'sha512')
        .toString('hex');
};

module.exports = mongoose.model('User', UserSchema);
