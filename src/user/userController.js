const userService = require('./userService');
const response = require('../../services/responseService');

/**
 * add new user to the system
 * @param {*} req
 * @param {*} res
 */
module.exports.newUser = async (req, res) => {
    try {
        let user = await userService.createUser(req.body);
        response.successTokenWithData(user, res);
    } catch (error) {
        response.customError('' + error, res);
    }
};
/**
 * get all 
 */
module.exports.getOne = async (req, res) => {
    try {
        let items = await userService.getOne({_id:req.params.id})
        response.successWithData(items, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}
 module.exports.getAll = async (req, res) => {
    try {
        let items = await userService.getAll({})
        response.successWithData(items, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}
/**
 * user login to the system
 * @param {*} req
 * @param {*} res
 */
module.exports.login = async (req, res) => {
    try {
        let user = await userService.loginUser(req.body);
        response.successTokenWithData(user, res);
    } catch (error) {
        response.customError('' + error, res);
    }
};

/**
 * update item by id
 * @param {*} req
 * @param {*} res
 */
module.exports.update = async (req, res) => {
    try {
        let details = JSON.parse(JSON.stringify(req.body));
        console.log(req.params.id);
        let item = await userService
            .findByIdAndUpdate(req.params.id, details);
        if (item == null || item == undefined)
            response.customError("Invalid id", res)
        else
            response.successWithData(item, res);
    } catch (error) {
        response.customError('' + error, res);
    }
};
module.exports.updatePassword = async (req, res) => {
    try {
        let user = await userService.updatePassword(req.params.id,req.body);
        response.successTokenWithData(user, res);
    } catch (error) {
        response.customError('' + error, res);
    }
};

/**
 * remove item by id
 * @param {*} req
 * @param {*} res
 */
module.exports.remove = async (req, res) => {
    try {
        console.log(req.params.id);
        let item = await userService.delete(req.params.id);
        response.successWithData(item, res);
    } catch (error) {
        response.customError('' + error, res);
    }
};