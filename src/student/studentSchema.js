const joi = require('joi');

// updateValve registration Schema and validations to be done
module.exports.create = joi.object().keys({
  name: joi.string().required(),
  email: joi.string().email().required(),
  image_urls: joi.array().items(joi.string()),
  nic_number: joi.string().required(),
  gender: joi.string().required(),
  classes: joi.array().items(),
  conact_number: joi.string().required()
});
module.exports.email = joi.object().keys({
  id: joi.string().alphanum().min(24).max(24),
  date: joi.string().required(),
  class: joi.string().required(),
  class_id: joi.string().required(),
  payment_type: joi.string().required(),
  description: joi.string().required(),
  total: joi.string().required(),
  subject: joi.string().required(),
});

// mainValveID registration Schema and validations to be done
module.exports.id = joi.object().keys({
  _id: joi.string().alphanum().min(24).max(24)
});


// mainValveStatus registration Schema and validations to be done
module.exports.update = joi.object().keys({
  name: joi.string(),
  email: joi.string().email(),
  image_urls: joi.array().items(joi.string()),
  nic_number: joi.string(),
  gender: joi.string(),
  classes: joi.array().items(),
  conact_number: joi.string()
});