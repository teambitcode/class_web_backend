const studentService = require('./studentService');
const response = require('../../services/responseService');


/**
 * get all 
 */
module.exports.getAll = async (req, res) => {
    try {
        console.log(req.query);
        let items = await studentService.getAll(req.query);
        response.successWithData(items, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}
/**
 * get all 
 */
module.exports.getAllPayments = async (req, res) => {
    try {
        console.log(req.query);
        let items = await studentService.getAllPaymentDetails(req.query);
        response.successWithData(items, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}

/**
 * get one item
 */
module.exports.findOne = async (req, res) => {
    try {
        let item = await studentService.findOne(req.body)
        response.successWithData(item, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}
/**
 * get one item
 */
module.exports.findOneByEmail = async (req, res) => {
    try {
        let item = await studentService.findOneByEmail(req.params.id)
        response.successWithData(item, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}
/**
 * create item
 */
module.exports.create = async (req, res) => {
    try {
        let item = await studentService.create(req.body);
        response.successWithData(item, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}
/**
 * create item
 */
module.exports.sendEmail = async (req, res) => {
    try {
        let item = await studentService.emailSending(req.body);
        response.successWithData(item, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}

/**
 * update item by id
 * @param {*} req
 * @param {*} res
 */
module.exports.update = async (req, res) => {
    try {
        let details = JSON.parse(JSON.stringify(req.body));
        delete details.id;
        delete details._id;
        delete details.createdAt;
        delete details.updatedAt;
        delete details.__v;
        console.log(req.params.id);
        let item = await studentService
            .findByIdAndUpdate(req.params.id, details);
        if (item == null || item == undefined)
            response.customError("Invalid id", res)
        else
            response.successWithData(item, res);
    } catch (error) {
        response.customError('' + error, res);
    }
};

/**
 * remove item by id
 * @param {*} req
 * @param {*} res
 */
module.exports.remove = async (req, res) => {
    try {
        console.log(req.params.id);
        let item = await studentService.delete(req.params.id);
        response.successWithData(item, res);
    } catch (error) {
        response.customError('' + error, res);
    }
};
