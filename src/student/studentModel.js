const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const studentSchema = new Schema(
    {
        name: {
            type: String,
            required: true
        },
        email: {
            type: String,
            required: true,
        },
        nic_number: {
            type: String,
            required: true,
        },
        gender: { type: String },
        conact_number: { type: String },
        classes: [
            {
                class_id: {
                    type: Schema.Types.ObjectId,
                    ref: "classes"
                },
                group_id: {
                    type: Schema.Types.ObjectId,
                    ref: "groups"
                }
            }
        ]

    }, { timestamps: true }
);
// studentSchema.index({ student: "2dsphere" });
module.exports = mongoose.model('students', studentSchema);
