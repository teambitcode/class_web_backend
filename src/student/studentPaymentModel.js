const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const studentPaymentSchema = new Schema(
    {
        student_id: {
            type: Schema.Types.ObjectId,
            ref: "students"
        },
        price: {
            type: String,
            required: true,
        },
        class: {
            type: String,
            required: true,
        },
        class_id: {
            type: Schema.Types.ObjectId,
            ref: "groups"
        },
        recipt_id: {
            type: String,
            required: true,
        }
        ,
        date: {
            type: String,
            required: true,
        }
    }, { timestamps: true }
);
// studentSchema.index({ student: "2dsphere" });
module.exports = mongoose.model('student_payment', studentPaymentSchema);
