const studentModel = require('./studentModel');
const studentPaymentModel = require('./studentPaymentModel');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var moment = require('moment-timezone');
const momentFormat = require('moment');
var transporter = nodemailer.createTransport(smtpTransport({
    service: 'gmail',
    auth: {
        user: 'teambitcode@gmail.com',
        pass: 'ypfczdglxdakvlml'
    }
}));
function sendMail(mailOptions,id) {
    return new Promise((resolve, reject) => {
        console.log('in Email sent');
        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log(error);
                resolve(false);
                // res.send('error') // if error occurs send error as response to client
            }
            else {
                console.log('Email sent: ' + info.response);
                resolve({id:id});
                // res.send('Sent Successfully')//if mail is sent successfully send Sent successfully as response
            }
        });
    });
}
/**
 * find from the system
 */
module.exports.getAll = (condition) => {
    return new Promise((resolve, reject) => {
        studentModel.find(condition).populate('classes.class_id').populate('classes.group_id').exec((err, student) => {
            err ? reject(err) : resolve(student);
        });
    })
};
/**
 * find from the system
 */
module.exports.getAllPaymentDetails = (condition) => {
    return new Promise((resolve, reject) => {
        studentPaymentModel.find(condition).populate('class_id').populate('student_id').exec((err, student) => {
            err ? reject(err) : resolve(student);
        });
    })
};

/**
 * remove  from the system
 */
module.exports.delete = (id) => {
    return new Promise((resolve, reject) => {
        studentModel.findOneAndDelete({ _id: id },
            async (err, student) => {
                if (err)
                    reject(err);
                else if (student != null || student != undefined) {
                    resolve("successfully deleted");
                } else
                    reject("Invalid id");
            });
    })
}
/**
 * register new to the system
 */
module.exports.emailSending = async (body) => {
    return new Promise((resolve, reject) => {
        studentModel.findOne({ _id: body.id }, async function (err, studentData) {
            console.log(studentData);
            console.log(err);
            if (err) {
                reject(err);
            }
            else if (studentData != undefined && studentData != null) {
                opt = createPaymentMailOptions(studentData, body);
                var res = await sendMail(opt.mailOptions);
                console.log("&***********************");
                console.log("&***********************");
                console.log("&***********************");
                console.log("&***********************");
                if (res) {

                    const studentPayment = new studentPaymentModel();
                    studentPayment.student_id = body.id;
                    studentPayment.price = body.total;
                    studentPayment.recipt_id = opt.id;
                    studentPayment.class = body.class;
                    studentPayment.class_id = body.class_id;
                    studentPayment.date = body.date;

                    studentPayment.save(async (error, studentCb) => {
                        if (error) {
                            resolve('Email Sent but record not saved');
                        } else {
                            resolve('Email Sent');
                        }
                    });

                } else {
                    resolve('Email Sending Error!');
                }
            } else {
                reject("student not exists");
            }
        });
    });
}
function generateUniqueString() {
    var ts = String(new Date().getTime()),
        i = 0,
        out = '';

    for (i = 0; i < ts.length; i += 2) {
        out += Number(ts.substr(i, 2)).toString(36);
    }

    return ('TeamBitCode-' + out);
}
function createPaymentMailOptions(user, body) {
    var randomId = generateUniqueString();
    if (!randomId) {
        randomId = generateUniqueString();
    }
    var emailData = {
        "id": randomId,
        "date": body.date,
        "name": user.name,
        "email": user.email,
        "mobile": user.conact_number,
        "nic": user.nic_number,
        "class": body.class,
        "payment_type": 'Bank Deposit',
        "description": body.description,
        "total": body.total,
        "subject": body.subject
    }
    console.log(emailData);
    var htmlTemp = getInvoiceTemplateWithoutRows(emailData);

    var mailOptions = {
        from: 'teambitcode@gmail.com',//replace with your email
        to: user.email,//replace with your email
        subject: body.subject,
        html: htmlTemp.data
    };
    return {mailOptions:mailOptions,id:htmlTemp.id};
}
function getInvoiceTemplateWithoutRows(data) {


    return {data:`<!DOCTYPE html>
    <html>
    <head>
    
      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <title>Email Receipt</title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <style type="text/css">
      
    body{
    
      font-family: 'Roboto', sans-serif;
    }
    ::selection {background: #f31544; color: #FFF;}
    ::moz-selection {background: #f31544; color: #FFF;}
    .clearfix::after {
        content: "";
        clear: both;
        display: table;
    }
    .col-left {
        float: left;
    }
    .col-center {
    margin: auto;
        padding: 10px;
    margin-left: 152px !important;
    }
    .col-right {
        float: right;
    }
    h1{
      font-size: 1.5em;
      color: #444;
    }
    h2{font-size: .9em;}
    h3{
      font-size: 1.2em;
      font-weight: 300;
      line-height: 2em;
    }
    p{
      font-size: .75em;
      color: #666;
      line-height: 1.2em;
    }
    a {
        text-decoration: none;
        color: #00a63f;
    }
    
    #invoiceholder{
      width:100%;
      height: 100%;
      padding: 50px 0;
    }
    #invoice{
      position: relative;
      margin: 0 auto;
      width: 700px;
         background: #f8f8f9;
    }
    
    [id*='invoice-']{ /* Targets all id with 'col-' */
    /*  border-bottom: 1px solid #EEE;*/
    padding: 36px;
    }
    
    #invoice-top{border-bottom: 2px solid #00a63f;}
    #invoice-mid{min-height: 110px;}
    #invoice-bot{ min-height: 240px;}
    
    .logo{
        display: inline-block;
        vertical-align: middle;
            width: 191px;
        overflow: hidden;
    }
    .info{
        display: inline-block;
        vertical-align: middle;
        margin-left: 20px;
    }
    .logo img,
    .clientlogo img {
        width: 140%;
    }
    .clientlogo{
        display: inline-block;
        vertical-align: middle;
        width: 140px;
    }
    .clientinfo {
        display: inline-block;
        vertical-align: middle;
        margin-left: 20px
    }
    .title{
      float: right;
    }
    .title p{text-align: right;}
    #message{margin-bottom: 30px; display: block;}
    h2 {
        margin-bottom: 5px;
        color: #444;
    }
    .col-right td {
        color: #666;
        padding: 5px 8px;
        border: 0;
        font-size: 0.75em;
        border-bottom: 1px solid #eeeeee;
    }
    .col-right td label {
        margin-left: 5px;
        font-weight: 600;
        color: #444;
    }
    .cta-group a {
        display: inline-block;
        padding: 7px;
        border-radius: 4px;
        background: rgb(196, 57, 10);
        margin-right: 10px;
        min-width: 100px;
        text-align: center;
        color: #fff;
        font-size: 0.75em;
    }
    .cta-group .btn-primary {
        background: #00a63f;
    }
    .cta-group.mobile-btn-group {
        display: none;
    }
    table{
      width: 100%;
      border-collapse: collapse;
    }
    td{
        padding: 10px;
        border-bottom: 1px solid #cccaca;
        font-size: 0.70em;
        text-align: left;
    }
    
    .tabletitle th {
      border-bottom: 2px solid #ddd;
      text-align: right;
    }
    .tabletitle th:nth-child(2) {
        text-align: left;
    }
    th {
        font-size: 0.7em;
        text-align: left;
        padding: 5px 10px;
    }
    .item{width: 50%;}
    .list-item td {
        text-align: right;
    }
    .list-item td:nth-child(2) {
        text-align: left;
    }
    .total-row th,
    .total-row td {
        text-align: right;
        font-weight: 700;
        font-size: .75em;
        border: 0 none;
    }
    .table-main {
        
    }
    footer {
        border-top: 1px solid #eeeeee;;
        padding: 15px 20px;
    }
    .effect2
    {
      position: relative;
    }
    .effect2:before, .effect2:after
    {
      z-index: -1;
      position: absolute;
      content: "";
      bottom: 15px;
      left: 10px;
      width: 50%;
      top: 80%;
      max-width:300px;
      background: #777;
      -webkit-box-shadow: 0 15px 10px #777;
      -moz-box-shadow: 0 15px 10px #777;
      box-shadow: 0 15px 10px #777;
      -webkit-transform: rotate(-3deg);
      -moz-transform: rotate(-3deg);
      -o-transform: rotate(-3deg);
      -ms-transform: rotate(-3deg);
      transform: rotate(-3deg);
    }
    .effect2:after
    {
      -webkit-transform: rotate(3deg);
      -moz-transform: rotate(3deg);
      -o-transform: rotate(3deg);
      -ms-transform: rotate(3deg);
      transform: rotate(3deg);
      right: 10px;
      left: auto;
    }
    @media screen and (max-width: 767px) {
        h1 {
            font-size: .9em;
        }
        #invoice {
            width: 100%;
        }
        #message {
            margin-bottom: 20px;
        }
        [id*='invoice-'] {
            padding: 20px 10px;
        }
        .logo {
            width: 140px;
        }
        .title {
            float: none;
            display: inline-block;
            vertical-align: middle;
            margin-left: 40px;
        }
        .title p {
            text-align: left;
        }
        .col-left,
        .col-right {
            width: 100%;
        }
        .table {
            margin-top: 20px;
        }
        #table {
            white-space: nowrap;
            overflow: auto;
        }
        td {
            white-space: normal;
        }
        .cta-group {
            text-align: center;
        }
        .cta-group.mobile-btn-group {
            display: block;
            margin-bottom: 20px;
        }
         /*==================== Table ====================*/
        .table-main {
            border: 0 none;
        }  
          .table-main thead {
            border: none;
            clip: rect(0 0 0 0);
            height: 1px;
            margin: -1px;
            overflow: hidden;
            padding: 0;
            position: absolute;
            width: 1px;
          }
          .table-main tr {
            border-bottom: 2px solid #eee;
            display: block;
            margin-bottom: 20px;
          }
          .table-main td {
            font-weight: 700;
            display: block;
            padding-left: 40%;
            max-width: none;
            position: relative;
            border: 1px solid #cccaca;
            text-align: left;
          }
          .table-main td:before {
            /*
            * aria-label has no advantage, it won't be read inside a table
            content: attr(aria-label);
            */
            content: attr(data-label);
            position: absolute;
            left: 10px;
            font-weight: normal;
            text-transform: uppercase;
          }
        .total-row th {
            display: none;
        }
        .total-row td {
            text-align: left;
        }
        footer {text-align: center;}
    }
    
     </style>
    <html>
    <head>
    
      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <title>Email Receipt</title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <style type="text/css">
      
    
      </style>
    
    </head>
    <body>
      <div id="invoiceholder">
      <div id="invoice" style="    padding: 46px !important;" class="effect2">
        
        <div id="invoice-top">
          <div class="logo">
          <h2 style="    font-size: 30px !important;
        font-family: unset  !important;
        color: #001fff  !important">TeamBitCode</h2>
          <p style="font-weight: 700;margin-top: -7px;">&nbsp;&nbsp;Simplicity is more creative</p>
          </div>
          <div class="title">
            <h1>Receipt #<span class="invoiceVal invoice_num">`+ data.id + `</span></h1>
            <p>Date: <span id="invoice_date">`+ data.date + `</span><br></span>
            </p>
          </div><!--End Title-->
        </div><!--End InvoiceTop-->
    
    
        
        <div id="invoice-mid">   
          <div id="message">
            <h2>Hi Student,</h2>
            <p>We have received your payment.</p>
          </div>
   
            <div class="clearfix">
                <div class="col-center">
                    <div class="clientlogo"><img style="    margin-left: -35px;width: 114% !important; margin-right: 5px !important;" src="https://i.dlpng.com/static/png/5149711-paid-stamp-png-101-images-in-collection-page-1-paid-stamp-png-476_177_preview.png" alt="Sup" /></div>
                    <div class="clientinfo" style="margin-left: 70px !important;">
                        <h2 id="supplier">`+ data.name + `</h2>
                        <p><span id="address">`+ data.email + `</span><br>
                        <span id="city">`+ data.mobile + `</span><br>
                        <span id="country">Class</span> - <span id="zip">`+ data.class + `</span><br>
                        <span id="country1">Address</span> - <span id="zip">---</span><br><br></p>
                    </div>
                </div>
                
            </div>       
        </div><!--End Invoice Mid-->
        
        <div id="invoice-bot">
        <hr>
          <div id="table">
            <table class="table-main">
            
              <tr class="list-item">
                <td data-label="Type" class="tableitem">`+ data.payment_type + `</td>
                <td data-label="Description" class="tableitem">`+ data.description + `</td>
                <td data-label="Price" class="tableitem">`+ data.total + `</td>
                </tr>
             
                <tr class="list-item total-row">
                    <th colspan="2" class="tableitem">Total</th>
                    <td data-label="Grand Total" class="tableitem" style=" border-bottom: 3px double; ">`+ data.total + `</td>
                </tr>
            </table>
          </div><!--End Table-->
          <div style="    margin-left: 28px;">
    
            <p style="    align-items: center !important;
        color: #00086b !important;
        font-family: cursive;
        font-size: small;"><b>We at TeamBiCode know you had many options to choose from, we thank you for choosing us.</b></p>
          </div>
          
        </div><!--End InvoiceBot-->
        <footer>
          <div id="legalcopy" class="clearfix">
            <p class="col-right">Our facebook page:
                <span class="email"><a href="https://www.facebook.com/TeamBitCode">https://www.facebook.com/TeamBitCode</a></span>
            </p>
          </div>
        </footer>
      </div><!--End Invoice-->
    </div><!-- End Invoice Holder-->
      
      
    
    </body>
    </html>`,id:data.id};
}
/**
 * register new to the system
 */
module.exports.create = async (body) => {
    return new Promise((resolve, reject) => {
        studentModel.findOne({ name: body.name }, async function (err, studentData) {
            console.log(studentData);
            console.log(err);
            if (err) {
                reject(err);
            }
            else if (studentData == undefined || studentData == null) {
                const student = new studentModel();
                student.name = body.name;
                student.email = body.email;
                student.nic_number = body.nic_number;
                student.gender = body.gender;
                student.conact_number = body.conact_number;
                student.classes = body.classes;

                student.save(async (error, studentCb) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve(studentCb);
                    }
                });
            } else {
                reject("student already exists");
            }
        });
    });
}

/**
 * find  from the system by id
 */
module.exports.findOne = (body) => {
    return new Promise((resolve, reject) => {
        studentModel.findOne(body).populate('classes.class_id').populate('classes.group_id')
            .exec((err, data) => {
                err ? reject(err) : resolve(data);
            });
    })
};
/**
 * find  from the system by id
 */
module.exports.findOneByEmail = (id) => {
    return new Promise((resolve, reject) => {
        studentModel.findOne({ email: id }).populate('classes.class_id').populate('classes.group_id')
            .exec((err, data) => {
                err ? reject(err) : resolve(data);
            });
    })
};

/**
 * find and update from the system by id
 */
module.exports.findByIdAndUpdate = async (id, updateObject) => {
    return new Promise(async (resolve, reject) => {
        try {
            studentModel.findOne({ _id: id }, function (err1, studentData) {
                console.log(id);
                console.log(updateObject);
                if (err1) {
                    reject(err1);
                }
                else if (studentData != undefined || studentData != null) {
                    studentModel.update({ _id: id }, { $set: updateObject }).exec((err, data) => {
                        err ? reject(err) : resolve(data);
                    });
                } else {
                    reject("student ID not found");
                }

            });
        } catch (error) {
            reject("Something went wrong");
        }
    })
};