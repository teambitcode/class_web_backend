const joi = require('joi');

// updateValve registration Schema and validations to be done
module.exports.create = joi.object().keys({
  name: joi.string().required(),
  class_id: joi.string().required(),
  language: joi.string().required(),
  duration: joi.number().required(),
  number_of_weeks: joi.number().required(),
  description:joi.string().required(),
  img:joi.string().required(),
  content:joi.array().required(),
  active: joi.boolean().required()
});

// mainValveID registration Schema and validations to be done
module.exports.id = joi.object().keys({
  _id: joi.string().alphanum().min(24).max(24)
});


// mainValveStatus registration Schema and validations to be done
module.exports.update = joi.object().keys({
  name: joi.string(),
  class_id: joi.string(),
  language: joi.string(),
  duration: joi.number(),
  number_of_weeks: joi.number(),
  description:joi.string(),
  img:joi.string(),
  content:joi.array(),
  active: joi.boolean()
});