const classesModel = require('./classesModel');
/**
 * find from the system
 */
module.exports.getAll = (condition) => {
    return new Promise((resolve, reject) => {
        classesModel.find(condition).exec((err, classes) => {
            err ? reject(err) : resolve(classes);
        });
    })
};

/**
 * remove  from the system
 */
module.exports.delete = (id) => {
    return new Promise((resolve, reject) => {
        classesModel.findOneAndDelete({ _id: id },
            async (err, classes) => {
                if (err)
                    reject(err);
                else if (classes != null || classes != undefined) {
                    resolve("successfully deleted");
                } else
                    reject("Invalid id");
            });
    })
}
/**
 * register new to the system
 */
module.exports.create = async (body) => {
    return new Promise((resolve, reject) => {
        classesModel.findOne({ name: body.name }, async function (err, classesData) {
            console.log(classesData);
            console.log(err);
            if (err) {
                reject(err);
            }
            else if (classesData == undefined || classesData == null) {
                const classes = new classesModel();
                classes.name = body.name;
                classes.class_id = body.class_id;
                classes.language = body.language;
                classes.duration = body.duration;
                classes.number_of_weeks = body.number_of_weeks;
                classes.description = body.description;
                classes.img = body.img;
                classes.content = body.content;
                classes.active = body.active;

                classes.save(async (error, classesCb) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve(classesCb);
                    }
                });
            } else {
                reject("classes already exists");
            }
        });
    });
}

/**
 * find  from the system by id
 */
module.exports.findOne = (id) => {
    return new Promise((resolve, reject) => {
        classesModel.findOne({ _id: id })
            .exec((err, data) => {
                err ? reject(err) : resolve(data);
            });
    })
};


/**
 * find and update from the system by id
 */
module.exports.findByIdAndUpdate = async (id, updateObject) => {
    return new Promise(async (resolve, reject) => {
        try {
            classesModel.findOne({ _id: id }, function (err1, classesData) {
                console.log(id);
                console.log(updateObject);
                if (err1) {
                    reject(err1);
                }
                else if (classesData != undefined || classesData != null) {
                    classesModel.update({ _id: id }, { $set: updateObject }).exec((err, data) => {
                        err ? reject(err) : resolve(data);
                    });
                } else {
                    reject("classes ID not found");
                }

            });
        } catch (error) {
            reject("Something went wrong");
        }
    })
};