const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const classesSchema = new Schema(
    {
        class_id: {
            type: String,
            required: true
        },
        name: {
            type: String,
            required: true
        },
        language: {
            type: String,
            required: true,
        },
        duration: {
            type: Number,
            required: true,
        },
        img: {
            type: String,
            required: true,
        },
        content: {
            type: [],
            required: true,
        },
        description: {
            type: String,
            required: true,
        },
        active: {
            type: Boolean,
            required: true,
        },
        number_of_weeks: { type: Number, required: true }

    }, { timestamps: true }
);
// classesSchema.index({ classes: "2dsphere" });
module.exports = mongoose.model('classes', classesSchema);
