const classesService = require('./classesService');
const response = require('../../services/responseService');
const smsService = require('../../services/smsService');


/**
 * get all 
 */
module.exports.getAll = async (req, res) => {
    try {
        console.log(req.query);
        let items = await classesService.getAll(req.query);
        response.successWithData(items, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}

/**
 * get one item
 */
module.exports.findOne = async (req, res) => {
    try {
        let item = await classesService.findOne(req.params.id)
        response.successWithData(item, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}

/**
 * create item
 */
module.exports.create = async (req, res) => {
    try {
        let item = await classesService.create(req.body);
        response.successWithData(item, res)
    } catch (error) {
        response.customError('' + error, res)
    }
}

/**
 * update item by id
 * @param {*} req
 * @param {*} res
 */
module.exports.update = async (req, res) => {
    try {
        let details = JSON.parse(JSON.stringify(req.body));
        let item = await classesService
            .findByIdAndUpdate(req.params.id, details);
        if (item == null || item == undefined)
            response.customError("Invalid id", res)
        else
            response.successWithData(item, res);
    } catch (error) {
        response.customError('' + error, res);
    }
};
/**
 * update item by id
 * @param {*} req
 * @param {*} res
 */
module.exports.sendSms = async (req, res) => {
    try {
        let details = JSON.parse(JSON.stringify(req.body));
        console.log(details);
        let item = await smsService
            .sendSms(details.numbers);
            console.log(item);
        if (item == null || item == undefined)
            response.customError("Invalid id", res)
        else
            response.successWithData(item, res);
    } catch (error) {
        response.customError('' + error, res);
    }
};

/**
 * remove item by id
 * @param {*} req
 * @param {*} res
 */
module.exports.remove = async (req, res) => {
    try {
        console.log(req.params.id);
        let item = await classesService.delete(req.params.id);
        response.successWithData(item, res);
    } catch (error) {
        response.customError('' + error, res);
    }
};
