'use strict';
// Import Express
const express = require('express');
// user router
const router = express.Router();
// Import body parser
const bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json({ limit: '50mb' }));
router.use(bodyParser.json());

// import  controllers
const userController = require('../src/user/userController');
const newStudentController = require('../src/new_students/newStudentsController');
const studentController = require('../src/student/studentController');
const classController = require('../src/classes/classesController');
const groupsController = require('../src/groups/groupsController');


// import validator Schemas
const userSchema = require('../src/user/userSchema');
const newStudentSchema = require('../src/new_students/newStudentsSchema');
const studentSchema = require('../src/student/studentSchema');
const classSchema = require('../src/classes/classesSchema');
const groupsSchema = require('../src/groups/groupsSchema');

// import Validator class
const validator = require('../services/validator');

//user routes
router.route('/user/get/:id')
    .get(validator.validateHeader(), userController.getOne);
router.route('/user/getAll')
    .get(validator.validateHeader(userSchema.id), userController.getAll);
router.route('/user/new')
    .post(validator.validateBody(userSchema.newUser), userController.newUser);
router.route('/user/login')
    .post(validator.validateBody(userSchema.login), userController.login);
router.route('/user/update/:id')
    .patch(validator.validateBodyWithToken(userSchema.update), userController.update);
router.route('/user/update/password/:id')
    .post(validator.validateBodyWithToken(userSchema.password), userController.updatePassword);
router.route('/user/delete/:id')
    .delete(validator.validateHeader(userSchema.id), userController.remove);

//student routes
router.route('/student/new')
    .post(validator.validateBodyWithToken(studentSchema.create), studentController.create);
router.route('/student/getAll')
    .get(validator.validateHeader(studentSchema.id), studentController.getAll);
router.route('/student/remove/:id')
    .delete(validator.validateHeader(studentSchema.id), studentController.remove);
router.route('/student/update/:id')
    .patch(validator.validateBodyWithToken(studentSchema.update), studentController.update);
router.route('/student/get-by-email/:id')
    .get(studentController.findOneByEmail);
router.route('/student/payment-email')
    .post(validator.validateBody(studentSchema.email), studentController.sendEmail);
router.route('/student/payment-email')
    .get(studentController.getAllPayments);

//new student routes
router.route('/new-student/new')
    .post(validator.validateBody(newStudentSchema.create), newStudentController.create);
router.route('/new-student/getAll')
    .get(validator.validateHeader(newStudentSchema.id), newStudentController.getAll);
router.route('/new-student/remove/:id')
    .delete(validator.validateHeader(newStudentSchema.id), newStudentController.remove);
router.route('/new-student/update/:id')
    .patch(validator.validateBodyWithToken(newStudentSchema.update), newStudentController.update);

//groups routes
router.route('/groups/new')
    .post(validator.validateBodyWithToken(groupsSchema.create), groupsController.create);
router.route('/groups/getAll')
    .get(validator.validateHeader(groupsSchema.id), groupsController.getAll);
router.route('/groups/remove/:id')
    .delete(validator.validateHeader(groupsSchema.id), groupsController.remove);

router.route('/groups/remove-student')
    .delete(groupsController.removeFromGroup);
router.route('/groups/remove-student/delete-state')
    .delete(groupsController.removeFromGroupStudentInDeleteState);
router.route('/groups/update/:id')
    .patch(validator.validateBodyWithToken(groupsSchema.update), groupsController.update);

//classes routes
router.route('/classes/new')
    .post(validator.validateBodyWithToken(classSchema.create), classController.create);
router.route('/classes/getAll')
    .get(classController.getAll);
router.route('/classes/get/:id')
    .get(classController.findOne);
router.route('/classes/remove/:id')
    .delete(validator.validateHeader(classSchema.id), classController.remove);
router.route('/classes/update/:id')
    .patch(validator.validateBodyWithToken(classSchema.update), classController.update);

router.route('/send/sms')
    .post(classController.sendSms);


module.exports = router;
